package threadpool;

import java.util.concurrent.TimeUnit;

public interface TreadPool<R> {

    void open();

    boolean isOpen();

    void close();

    R acquire();

    R acquire(long timeout, TimeUnit timeUnit) throws InterruptedException;

    void release(R resource);

    boolean add(R resource);

    boolean remove(R resource) throws InterruptedException;
}
