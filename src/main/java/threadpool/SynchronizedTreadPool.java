package threadpool;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SynchronizedTreadPool<R> {

    private volatile boolean open;

    private Set<R> resources = ConcurrentHashMap.newKeySet();
    private Set<R> resourcesForRemoval = ConcurrentHashMap.newKeySet();
    private Set<R> lockedResources = ConcurrentHashMap.newKeySet();

    private LinkedBlockingQueue<R> freeResources = new LinkedBlockingQueue<>();

    private final ReadWriteLock modifyResourceLock = new ReentrantReadWriteLock();

    private final Lock modifyResourceReadLock = modifyResourceLock.readLock();
    private final Lock modifyResourceWriteLock = modifyResourceLock.writeLock();

    private final Condition resourceReleasedCondition = modifyResourceWriteLock.newCondition();

    private final ExecutorService releaseExecutor = Executors.newSingleThreadExecutor();

    public SynchronizedTreadPool() {
    }

    public SynchronizedTreadPool(List<R> resources, boolean open) {
        this.resources.addAll(resources);
        this.freeResources.addAll(resources);
        this.open = open;
    }

    public SynchronizedTreadPool(List<R> resources) {
        this(resources, false);
    }

    public synchronized void open() {
        open = true;
    }

    public boolean isOpen() {
        return open;
    }

    public synchronized void close() throws InterruptedException {
        open = false;
        try {
            modifyResourceWriteLock.lock();
            while (!lockedResources.isEmpty()) {
                resourceReleasedCondition.await();
            }
        } finally {
            modifyResourceWriteLock.unlock();
        }
    }

    public synchronized void closeNow() {
        open = false;
    }

    public R acquire() throws InterruptedException {
        if (open) {
            try {
                modifyResourceReadLock.lock();
                if (!open) {
                    return null;
                }
                R acquiredResource = freeResources.take();
                lockedResources.add(acquiredResource);
                return acquiredResource;
            } finally {
                modifyResourceReadLock.unlock();
            }
        } else {
            return null;
        }
    }

    public R acquire(long timeout, TimeUnit timeUnit) throws InterruptedException {
        if (open) {
            try {
                modifyResourceReadLock.lock();
                if (!open) {
                    return null;
                }
                R acquiredResource = freeResources.poll(timeout, timeUnit);
                lockedResources.add(acquiredResource);
                return acquiredResource;
            } finally {
                modifyResourceReadLock.unlock();
            }
        } else {
            return null;
        }
    }

    public void release(R resource) throws InterruptedException {
        if (!lockedResources.remove(resource)) {
            return;
        }

        try {
            modifyResourceReadLock.lock();
            if (lockedResources.remove(resource)) {
                if (resourcesForRemoval.isEmpty() || !resourcesForRemoval.remove(resource)) {
                    freeResources.put(resource);
                } else {
                    releaseExecutor.submit(() -> {
                        modifyResourceWriteLock.lock();
                        resourceReleasedCondition.signalAll();
                        modifyResourceWriteLock.unlock();
                    });
                }
            }
        } finally {
            modifyResourceReadLock.unlock();
        }
    }

    public synchronized boolean add(R resource) {
        if (!resources.add(resource)) {
            return false;
        }
        return freeResources.add(resource);
    }

    public synchronized boolean remove(R resource) throws InterruptedException {
        if (!resources.remove(resource)) {
            return false;
        }

        if (freeResources.remove(resource)) {
            return true;
        }

        modifyResourceWriteLock.lock();
        try {
            resourcesForRemoval.add(resource);
            while (resourcesForRemoval.contains(resource)) {
                resourceReleasedCondition.await();
            }
        } finally {
            modifyResourceWriteLock.unlock();
        }
        return true;
    }

}
